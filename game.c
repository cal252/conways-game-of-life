#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DEFAULT_COL 80
#define DEFAULT_ROW 24
#define DEFAULT_ITERATIONS 40
#define SLEEP_TIME 0.5

void print_iteration(int** arr, int col, int row);
void generate_iteration(int** arr, int col, int row);
void update_iteration(int** arr, int** temp, int row, int col);
void free_memory(int** array, int row);

int main(int argc, char** argv)
{
	int col;
	int row;
	int iterations;
	int** board;
	int i;
	
	switch(argc)
	{
		case 1:
			col = DEFAULT_COL;
			row = DEFAULT_ROW;
			iterations = DEFAULT_ITERATIONS;
			break;
		case 2:
			col = atoi(argv[1]);
			row = DEFAULT_ROW;
			iterations = DEFAULT_ITERATIONS;
			break;
		case 3:
			col = atoi(argv[1]);
			row = atoi(argv[2]);
			iterations = DEFAULT_ITERATIONS;
			break;
		default:
			col = atoi(argv[1]);
			row = atoi(argv[2]);
			iterations = atoi(argv[3]);
			break;
	}
	board = malloc(row * sizeof(int*));
	for(i=0; i<col; i++)
	{
		board[i] = calloc(col, sizeof(int));
	}
	// board[34][30] = 1;
	// board[35][30] = 1;
	// board[36][30] = 1;

	// board[28][30] = 1;
	// board[29][30] = 1;
	// board[30][30] = 1;
	// board[30][29] = 1;
	// board[29][28] = 1;

	board[34][30] = 1;
	board[35][30] = 1;
	board[36][30] = 1;

	board[28][30] = 1;
	board[29][30] = 1;
	board[30][30] = 1;
	board[28][29] = 1;
	board[29][28] = 1;

	//printf("%d %d\n", col, row);
	for(i=0; i<iterations; i++)
	{
		generate_iteration(board, row, col);
		print_iteration(board, row, col);
		usleep(100000);
	}
	free_memory(board, row);
	return 0;
}

void print_iteration(int** arr, int row, int col)
{
	int i;
	int j;
	for(i=0; i<row; i++)
	{
		for(j=0; j<col; j++)
		{
			if(arr[i][j] == 0)
			{
				printf(" ");
			}
			else
			{
				printf("$");
			}
		}
		printf("\n");
	}
}

void generate_iteration(int** arr, int row, int col)
{
	int** temp;
	int i;
	int j;
	int adj;

	temp = malloc(row * sizeof(int*));
	for(i=0; i<col; i++)
	{
		temp[i] = calloc(col, sizeof(int));
	}

	for(i=0; i<row; i++)
	{
		for(j=0; j<col; j++)
		{
			adj = 0;
			//dead cells
			if(arr[i][j] == 0)
			{
				//check top row
				if(i == 0)
				{
					//top left corner
					if(j == 0)
					{
						adj += arr[i+1][j];
						adj += arr[i][j+1];
						adj += arr[i+1][j+1];
					}
					//top right corner
					else if(j == col - 1)
					{
						adj += arr[i+1][j];
						adj += arr[i][j-1];
						adj += arr[i+1][j-1];
					}
					//top row excluding corners
					else
					{
						adj += arr[i+1][j];
						adj += arr[i][j-1];
						adj += arr[i+1][j-1];
						adj += arr[i][j+1];
						adj += arr[i+1][j+1];
					}
				}
				//check bottom row
				else if(i == row - 1)
				{
					//bottom left corner
					if(j == 0)
					{
						adj += arr[i-1][j];
						adj += arr[i][j+1];
						adj += arr[i-1][j+1];
					}
					//bottom right corner
					else if(j == col - 1)
					{
						adj += arr[i-1][j];
						adj += arr[i][j-1];
						adj += arr[i-1][j-1];
					}
					//bottom row excluding corners
					else
					{
						adj += arr[i-1][j];
						adj += arr[i][j-1];
						adj += arr[i-1][j-1];
						adj += arr[i][j+1];
						adj += arr[i-1][j+1];
					}
				}
				//left column
				else if(j == 0)
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i-1][j+1];
					adj += arr[i+1][j+1];
					adj += arr[i][j+1];

				}
				//right column
				else if(j == 0)
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i-1][j-1];
					adj += arr[i+1][j-1];
					adj += arr[i][j-1];

				}
				//middle area
				else
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i][j+1];
					adj += arr[i][j-1];
					adj += arr[i-1][j-1];
					adj += arr[i+1][j-1];
					adj += arr[i-1][j+1];
					adj += arr[i+1][j+1];
				}

				//if cell has 3 alive neighbours then it becomes alive
				if(adj == 3)
				{
					temp[i][j] = 1;
				}
				//otherwise it stays dead
				else
				{
					temp[i][j] = 0;
				}
			}
			//alive cells
			else
			{
			//check top row
				if(i == 0)
				{
					//top left corner
					if(j == 0)
					{
						adj += arr[i+1][j];
						adj += arr[i][j+1];
						adj += arr[i+1][j+1];
					}
					//top right corner
					else if(j == col - 1)
					{
						adj += arr[i+1][j];
						adj += arr[i][j-1];
						adj += arr[i+1][j-1];
					}
					//top row excluding corners
					else
					{
						adj += arr[i+1][j];
						adj += arr[i][j-1];
						adj += arr[i+1][j-1];
						adj += arr[i][j+1];
						adj += arr[i+1][j+1];
					}
				}
				//check bottom row
				else if(i == row - 1)
				{
					//bottom left corner
					if(j == 0)
					{
						adj += arr[i-1][j];
						adj += arr[i][j+1];
						adj += arr[i-1][j+1];
					}
					//bottom right corner
					else if(j == col - 1)
					{
						adj += arr[i-1][j];
						adj += arr[i][j-1];
						adj += arr[i-1][j-1];
					}
					//bottom row excluding corners
					else
					{
						adj += arr[i-1][j];
						adj += arr[i][j-1];
						adj += arr[i-1][j-1];
						adj += arr[i][j+1];
						adj += arr[i-1][j+1];
					}
				}
				//left column
				else if(j == 0)
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i-1][j+1];
					adj += arr[i+1][j+1];
					adj += arr[i][j+1];

				}
				//right column
				else if(j == 0)
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i-1][j-1];
					adj += arr[i+1][j-1];
					adj += arr[i][j-1];

				}
				//middle area
				else
				{
					adj += arr[i-1][j];
					adj += arr[i+1][j];
					adj += arr[i][j+1];
					adj += arr[i][j-1];
					adj += arr[i-1][j-1];
					adj += arr[i+1][j-1];
					adj += arr[i-1][j+1];
					adj += arr[i+1][j+1];
				}

				//if live cell has 2/3 alive neighbours it stays alive
				if(adj == 2 || adj == 3)
				{
					temp[i][j] = 1;
				}
				//otherwise it dies
				else
				{
					temp[i][j] = 0;
				}
			}
		}
	}
	update_iteration(arr, temp, row, col);
	free_memory(temp, row);
}

void update_iteration(int** arr, int** temp, int row, int col)
{
	int i;
	int j;
	for(i=0; i<row; i++)
	{
		for(j=0; j<col; j++)
		{
			arr[i][j] = temp[i][j];
		}
	}
}

void free_memory(int** array, int row)
{
	int i;
	for(i=0; i<row; i++)
	{
		free(array[i]);
	}
	free(array);
}


