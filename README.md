# Conways Game of Life

A basic version of Conway's game of life

## How to use

Compile the program with a standard C compliler.

command line arguments are as follows:
- number of columns
- number of rows
- number of iterations

If any of these are not entered then defaults will be used.

The defaults are 80 columns, 24 rows and 40 iterations.

To change the starting state of the board edit the array elements. Currently this is done in lines 51-69. Changing a cell to 1 will make its initial state 'alive'.

## Original & information

wikipedia page detailing conways game of life rules and example patterns: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

Youtube video detailing what can be done/created with all the patterns: https://www.youtube.com/watch?v=C2vgICfQawE

## License
[The Unlicense](LICENSE)

